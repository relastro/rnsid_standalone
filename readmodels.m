function out = readmodels(direc)

files = dir([ direc '/*rho*.txt']);

for i = 1:length(files)
    
    command = ['grep xx2 ' direc '/' files(i).name ...
        ' | sed "s/xx2\*|//" > tempdata.txt'];
    system(command);
    
    data = load('tempdata.txt');
    
    s = size(data);
    
    out.rhoc(1:s(1),i) = data(:,1).*1.e-4;
    out.a_ratio(1:s(1),i) = data(:,2);
    out.R_e(1:s(1),i) = data(:,3);
    out.M_0(1:s(1),i) = data(:,4);
    out.M(1:s(1),i) = data(:,5);
    out.J(1:s(1),i) = data(:,6);
    out.P_a(1:s(1),i) = data(:,7);
    out.P_e(1:s(1),i) = data(:,8);
    out.t_d(1:s(1),i) = data(:,9);
    out.M_R(1:s(1),i) = data(:,10);
    out.beta(1:s(1),i) = data(:,11);
   
end


end