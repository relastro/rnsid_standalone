# SBernuzzi
# makefile for cactus_rns

BASE = $(shell /bin/pwd)
EXED = $(BASE)/$(EXECTBL)
OBJD = $(BASE)/obj
SRCD = $(BASE)/src
DOCD = $(BASE)/doc

vpath %.o $(OBJD)
vpath %.c $(SRCD)

EXECTBL  = rns
EXECTBL2 = rns_seq
ARCHIVE = cactus_rns.tgz
DOCrns 	= manual_rns_v2.0
DOCcctk = documentation_RNSIDthorn

SRCS = nrutil.c rnsid.c rnsid_util.c equil.c equil_util.c main.c 
OBJS = nrutil.o rnsid.o rnsid_util.o equil.o equil_util.o main.o

ARCFILES = Makefile src/*.c src/*.h doc/documentation_RNSIDthorn.tex doc/manual_rns_v2.0.tex doc/cactus.sty obj/

CC	= gcc
CCFLAGS = -lm


all: $(DOCrns) $(DOCcctk) $(ARCHIVE) $(EXECTBL) $(EXECTBL2)

$(EXECTBL2): $(OBJS) main_seq.o
	@echo ''
	@echo '-------------------------------------------------' 
	@echo ' Building the executable'
	cd $(OBJD); $(CC)  -o ../$(EXECTBL2) nrutil.o rnsid_util.o equil.o equil_util.o main_seq.o $(CCFLAGS)
	@echo ''
	@echo ' All Done !'

$(EXECTBL): $(OBJS)
	@echo ''
	@echo '-------------------------------------------------' 
	@echo ' Building the executable'
	cd $(OBJD); $(CC)  -o ../$(EXECTBL) $(OBJS) $(CCFLAGS)
	@echo ''
	@echo ' All Done !'

%.o $(OBJD)/%.o: %.c
	@echo '' 
	@echo '--------------------------------------------------'
	@echo ' Building' $*.o	
	cd $(OBJD); $(CC) -c $(CCFLAGS) $(SRCD)/$(*).c
	@echo ''	
	@echo ' Done'

$(ARCHIVE):
	@echo ''
	@echo '-------------------------------------------------' 
	@echo ' Building the tar archive'
	tar -zcvf $(ARCHIVE) $(ARCFILES)
	@echo ''	
	@echo ' Done'

$(DOCrns):	
	@echo ''
	@echo '-------------------------------------------------' 
	@echo ' Building the v 2.0 documentation'
	cd $(DOCD); latex $(DOCrns).tex; latex $(DOCrns).tex; dvips -o $(DOCrns).ps $(DOCrns).dvi
	@echo ''
	@echo ' Done !'

$(DOCcctk):	
	@echo ''
	@echo '-------------------------------------------------' 
	@echo ' Building the cactus-whisky thorn documentation '	
	cd $(DOCD); latex $(DOCcctk).tex; latex $(DOCcctk).tex; dvips -o $(DOCcctk).ps $(DOCcctk).dvi			
	@echo ''
	@echo ' Done !'

# DO NOT DELETE
