#!/bin/bash
#
# This is a simple demonstration script showing how to create a series of stars
# with different central density with RNSID. As rns_seq creates only sequences
# with different angular velocity, this can be helpful.
# Oct 2017, Sven Koeppel

mkdir -p rnsid-output

# central densities
r_start=0.001
r_step=0.0001
r_end=0.004

r_seq=$(seq $r_start $r_step $r_end | tr ',' '.')

echo "Creating $(echo "$r_seq" | wc -l) stars"

for r in $r_seq; do
	echo "Starting RNSID with r=$r"
	../rns -r$r -k123.647 > rnsid-output/rnsid__r_${r}.txt &
done

echo "Waiting for Jobs to be done (each typically 10sec)"
wait
