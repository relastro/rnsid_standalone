#!/bin/bash
# SvenK

# concatenate results to a CSV table
TABLE="results.csv"
echo "rhoc M" > $TABLE
cat rnsid-output/*.txt |  awk 'BEGIN{ORS=""} /rhoc\s*=/{print $4 "\t"} /M\s*=/{print $4 "\n"}' >> $TABLE

# plot that, here for convenience with gnuplot
gnuplot -persist <<-GNUPLOT

set title "RNSID series"
set xlabel "rhoc"
set ylabel "M"
set key autotitle columnhead
plot "results.csv" using "rhoc":"M"

GNUPLOT
