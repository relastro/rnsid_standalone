 /*@@
   @header    rnsid.h
   @date      Fri Dec  7 22:14:50 2001
   @author    Tom Goodale
   @desc 
   
   @enddesc
   @version $Header: /usr/local/share/PrNRG_CVSrepos/codes/cactus_rns/src/rnsid.h,v 1.2 2007/08/30 13:16:30 gcorv Exp $
 @@*/

#ifndef _RNSID_H_
#define _RNSID_H_ 1

#ifdef __cplusplus
extern "C" 
{
#endif

#define CCTK_REAL double
#define CCTK_INT int

void rnsid(          
          CCTK_REAL rho0_center,
          CCTK_REAL r_ratio,
          CCTK_REAL eos_k,
          CCTK_REAL Gamma_P,
          CCTK_REAL atm_factor,
          CCTK_REAL accuracy,
          char      zero_shift[20],
          char      save_2Dmodel[20],
          char      model2D_file[100],
          char      recover_2Dmodel[20],
          char      rotation_type[20],
          CCTK_REAL A_diff,
	  char eos_type[80],
	  char eos_file[80],
          CCTK_REAL *Omega_pt,
          CCTK_REAL *R_e_pt,
          CCTK_REAL *r_e_pt,
          CCTK_REAL *mass0_pt,
          CCTK_REAL cf,
          CCTK_INT RNS_lmax);
  
  
#ifdef __cplusplus
}
#endif

#endif /* _RNSID_H_ */
