#include <stdio.h>
#include <unistd.h>
#include "rnsid.h"

int main (int argc, char **argv) {
  
  /* ******************************************
     Default Values 
     ********************************************* */
  
  double OMEGApt[1], R_e_pt[1],r_e_pt[1],mass0_pt[1];
  double rho0_center       = 1.28e-3;                /* Central density */
  char   eos_type[80]      = "poly";                 /* {"poly" "tab"} */
  char   eos_file[80]      = "no EOS file specified";
  double eos_k             = 100.0;                  /* poly EOS K */
  double Gamma_P           = 2.0;                    /* poly EOS Gamma */
  char   rotation_type[20] = "diff";                 /* {"uniform" "diff"} */
  double A_diff            = 1.0;                    /* \hat{A} diff rot law */
  double r_ratio           = 1.00;                   /* axes ratio r_p/r_e */
  double accuracy          = 1.e-7;                  /* accuracy goal */
  char   save_2Dmodel[3]   = "no";                   /* save 2D output?*/
  char   model2D_file[80]  = "model2D.dat";          /*name of the file for the output*/ 
  double atm_factor        = 1.e-10;
  

  /* ******************************************
     .... a la charte
     ********************************************* */
  
  int opterr,c;
  opterr = 0;
  for(c = getopt(argc,argv,"+hur:a:t:f:k:g:d:e:o:n:");
      c != EOF;
      c = getopt(argc,argv,"+hur:a:t:f:k:g:d:e:o:n:")) {
    switch(c) {
    case 'r': /* Central density */
      sscanf(optarg,"%lf",&rho0_center);
      break;
    case 'a': /* axes ratio r_p/r_e */
      sscanf(optarg,"%lf",&r_ratio);
      break;
    case 't' : /* eos type */
      sscanf(optarg,"%s",&eos_type);
      break;
    case 'f' : /* eos TAB file */
      sscanf(optarg,"%s",&eos_file);
      break;
    case 'k' : /* poly EOS K */
      sscanf(optarg,"%lf",&eos_k);
      break;
    case 'g': /* poly EOS Gamma */
      sscanf(optarg,"%lf",&Gamma_P);
      break;
    case 'd': /* \hat{A} diff rot law par */
      sscanf(optarg,"%lf",&A_diff);
      break;
    case 'e': /* Accuracy goal */
      sscanf(optarg,"%lf",&accuracy);
      break;
    case 'h':
      printf("Help: ./rns -options<meaning> {default value} \n");
      printf("      -r<central density>     {1.28e-3}\n");
      printf("      -a<axes ratio rp/re>    {1.0}\n");
      printf("      -t<EOS type: poly/tab>  {poly} \n");
      printf("      -f<tab EOS file>        {no file} \n");
      printf("      -k<poly EOS K>          {100.0}\n");
      printf("      -g<poly EOS Gamma>      {2.0}\n");
      printf("      -d<{A} diff>            {1.0}\n");
      printf("      -e<accuracy goal>       {1e-7}\n");
      printf("      -u  (set uniform rotation)\n");
      printf("      -o<save 2D output?>     {no}\n");
      printf("      -n<output file name>   {model2D.dat}\n");
      return 0;
    case 'u':
      strcpy(rotation_type,"uniform");
      break;
    case 'o':
      sscanf(optarg,"%s",&save_2Dmodel);
      break;
    case 'n':
      sscanf(optarg,"%s",&model2D_file);
      break;
    default:
      break;
    }
  }
  
  /* ******************************************
     call rnsid
     ********************************************* */
  
  rnsid(rho0_center,r_ratio,eos_k,Gamma_P,atm_factor,accuracy,
	"no"    , /* char      zero_shift[20],      */
	save_2Dmodel,    
	model2D_file,   
	"no"    , /* char      recover_2Dmodel[20], */
	rotation_type,A_diff, 
	eos_type,eos_file,///////////////////////////////////////
	OMEGApt , /* CCTK_REAL *Omega_pt,           */
	R_e_pt  , /* CCTK_REAL *R_e_pt,             */
	r_e_pt  , /* CCTK_REAL *r_e_pt,             */
	mass0_pt, /* CCTK_REAL *mass0_pt,           */
	1.0     , /* CCTK_REAL cf,                  */
	10     ); /* CCTK_INT RNS_lmax)             */  
  
  return 0;
}
