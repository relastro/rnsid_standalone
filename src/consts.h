

/* LOW RESOLUTION (USE THIS ON LOW-MEMORY MACHINES) */

/*
#define MDIV 81
#define SDIV 151
*/


/* MAXIMUM RESOLUTION (NO POINT IN GOING BEYOND THAT DUE TO GIBBS ERRORS) */


#define MDIV 301 
#define SDIV 601


/* OTHER POSSIBLE CHOICES */ 

/*
#define MDIV 181
#define SDIV 301
*/

/*
#define MDIV 121
#define SDIV 341
*/

/* 
#define MDIV 221
#define SDIV 341
*/

/*
#define MDIV 221
#define SDIV 541
*/

/*
#define MDIV 161
#define SDIV 841
*/

/*#define MDIV 161*/ /*2201*/                     /* Number of angular grid points */
/* #define SDIV 181*/                      /* Number of radial grid points */
/*#define SDIV 501*/ /* 1001 */
/*#define SDIV 401 */
#define DM (1.0/(MDIV-1.0))          /* spacing in mu direction */ 
#define RDIV 900                     /* grid point in RK integration */ 
#define SMAX 0.9999                  /* maximum value of s-coordinate */  
#define DS (SMAX/(SDIV-1.0))         /* spacing in s-direction */

#define C 2.99792458e10                  /* speed of light in vacuum */
#define G 6.673e-8                  /* gravitational constant */ 
#define KAPPA (1.0e-15*C*C/G)        /* scaling factor */
#define KSCALE (KAPPA*G/(C*C*C*C))   /* another scaling factor */
#define MSUN 1.98892e33                /* Mass of Sun */
#define SQ(x) ((x)*(x))              /* square macro */
#define MB 1.66e-24                  /* baryon mass */
#define RMIN 1.0e-15                 /* use approximate TOV equations when
					computing spherical star and r<RMIN */

/*constants to convert from cgs to cactus units*/
#define cactusM 0.50278e-33
#define cactusL 0.677178e-5
#define cactusT 2.03012e5
#define cactusV 1.0/(cactusL*cactusL*cactusL)

#define MAXIT 1000
#ifndef PI
#define PI 3.1415926535              /* what else */
#endif
#include <float.h>
#include <limits.h>

#ifndef DBL_EPSILON
#define DBL_EPSILON 1e-15
#endif

#define ITMAX 100
#define EPS DBL_EPSILON
