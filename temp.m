

alpha_tmp   = model2D.data(:,1);
omega_tmp   = model2D.data(:,2);
gama_tmp    = model2D.data(:,3);
rho_pot_tmp = model2D.data(:,4);
energy_tmp  = model2D.data(:,5);
press_tmp   = model2D.data(:,6);
h_tmp       = model2D.data(:,7);
vel_sq_tmp  = model2D.data(:,8);
Omg_tmp     = model2D.data(:,9);

r_e_tmp = model2D.textdata{1};
r_e_str = r_e_tmp(1:18);
r_e = str2double(r_e_str);

s = 0.9999.*(((1:601)-1)./600);
mu = -(1-(0:300))./300;

r = (s.*r_e)./(1-s);
th = acos(mu);


for i = 1:601
    
    out.alpha(i,:) = alpha_tmp(1+301*(i-1):301*i);
    out.omega(i,:) = omega_tmp(1+301*(i-1):301*i);    
    out.gama(i,:) = gama_tmp(1+301*(i-1):301*i);
    out.rho_pot(i,:) = rho_pot_tmp(1+301*(i-1):301*i);
    out.energy(i,:) = energy_tmp(1+301*(i-1):301*i);
    out.pressure(i,:) = press_tmp(1+301*(i-1):301*i);
    out.hentalpy(i,:) = h_tmp(1+301*(i-1):301*i);
    out.vel_sq(i,:) = vel_sq_tmp(1+301*(i-1):301*i); 
    out.Omg(i,:) = Omg_tmp(1+301*(i-1):301*i); 
    
end

out.r_e = r_e;
out.r = r;
out.th = th;
out.s = s;
out.mu = mu;

clear r_e* r th mu s *_tmp i


